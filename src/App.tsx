import React from "react";
import { Provider } from "react-redux";
import { store } from "./store";
import { ProductsListPage } from "./pages/products";

export default function App() {
  return (
    <Provider store={store}>
      <React.StrictMode>
        <ProductsListPage />
      </React.StrictMode>
    </Provider>
  );
}
