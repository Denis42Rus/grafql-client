import {
  CategoriesType,
  NewProductType,
  ProductsType,
  SortingType,
} from "./types";

export const GET_PRODUCTS = "GET_PRODUCTS";
export const getProductsAction = () => ({
  type: "GET_PRODUCTS",
});

export const SET_PRODUCTS = "SET_PRODUCTS";
export const setProductsAction = (payload: ProductsType) => ({
  type: "SET_PRODUCTS",
  payload,
});

export const SET_PRODUCT = "SET_PRODUCT";
export const setProductAction = (payload: NewProductType) => ({
  type: "SET_PRODUCT",
  payload,
});

export const GET_PRODUCT = "GET_PRODUCT";
export const getProductAction = (payload: string) => ({
  type: "GET_PRODUCT",
  payload,
});

export const SET_ACTIVE_PRODUCT_UUID = "SET_ACTIVE_PRODUCT_UUID";
export const setActiveProductUuidAction = (payload: string) => ({
  type: "SET_ACTIVE_PRODUCT_UUID",
  payload,
});

export const ADD_PRODUCT = "ADD_PRODUCT";
export const addProductAction = () => ({
  type: "ADD_PRODUCT",
});

export const UPDATE_PRODUCT = "UPDATE_PRODUCT";
export const updateProductAction = () => ({
  type: "UPDATE_PRODUCT",
});

export const DELETE_PRODUCT = "DELETE_PRODUCT";
export const deleteProductAction = (payload: string) => ({
  type: "DELETE_PRODUCT",
  payload,
});

export const SET_ACTIVE_PAGE = "SET_ACTIVE_PAGE";
export const setActivePageAction = (payload: number) => ({
  type: "SET_ACTIVE_PAGE",
  payload,
});

export const SET_IS_PRODUCT_PAGE_OPEN = "SET_IS_PRODUCT_PAGE_OPEN";
export const setIsProductPageOpenAction = (payload: boolean) => ({
  type: "SET_IS_PRODUCT_PAGE_OPEN",
  payload,
});

export const GET_CATEGORIES = "GET_CATEGORIES";
export const getCategoriesAction = () => ({
  type: "GET_CATEGORIES",
});

export const SET_CATEGORIES = "SET_CATEGORIES";
export const setCategoriesAction = (payload: CategoriesType) => ({
  type: "SET_CATEGORIES",
  payload,
});

export const SET_SORTING = "SET_SORTING";
export const setSortingAction = (payload: SortingType) => ({
  type: "SET_SORTING",
  payload,
});

export const SET_FILTER = "SET_FILTER";
export const setFilterAction = (payload: string) => ({
  type: "SET_FILTER",
  payload,
});
