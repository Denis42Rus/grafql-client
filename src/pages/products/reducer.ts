import {
  SET_ACTIVE_PAGE,
  SET_ACTIVE_PRODUCT_UUID,
  SET_CATEGORIES,
  SET_FILTER,
  SET_IS_PRODUCT_PAGE_OPEN,
  SET_PRODUCT,
  SET_PRODUCTS,
  SET_SORTING,
} from "./actions";
import { ActionsTypes, ProductsStateType } from "./types";

const initialState: ProductsStateType = {
  products: [],
  categories: [],
  product: {},
  sorting: { field: "title", order: "asc" },
  page: 1,
  limit: 10,
  isProductPageOpen: false,
  activeProductUuid: "",
  filter: "",
};

function productReducer(state = initialState, action: ActionsTypes) {
  const { type, payload } = action;
  switch (type) {
    case SET_PRODUCTS:
      return {
        ...state,
        products: payload,
      };
    case SET_ACTIVE_PAGE:
      return {
        ...state,
        page: payload,
      };
    case SET_IS_PRODUCT_PAGE_OPEN:
      return {
        ...state,
        isProductPageOpen: payload,
      };
    case SET_CATEGORIES:
      return {
        ...state,
        categories: payload,
      };
    case SET_PRODUCT:
      return {
        ...state,
        product: payload,
      };
    case SET_ACTIVE_PRODUCT_UUID:
      return {
        ...state,
        activeProductUuid: payload,
      };
    case SET_SORTING:
      return {
        ...state,
        sorting: payload,
      };
    case SET_FILTER:
      return {
        ...state,
        filter: payload,
      };
    default: {
      return state;
    }
  }
}
export default productReducer;
