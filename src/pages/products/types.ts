import {
  SET_ACTIVE_PAGE,
  SET_ACTIVE_PRODUCT_UUID,
  SET_CATEGORIES,
  SET_FILTER,
  SET_IS_PRODUCT_PAGE_OPEN,
  SET_PRODUCT,
  SET_PRODUCTS,
  SET_SORTING,
} from "./actions";

export type ProductType = {
  category: string;
  id: string;
  price: number;
  title: string;
};

export type ProductsType = ProductType[];

export type NewProductType = {
  category?: string;
  price?: number;
  title?: string;
};

export type ProductFromApiType = {
  category: { title: string };
  id: string;
  price: number;
  title: string;
};

export type CategoryType = {
  id: string;
  title: string;
};

export type CategoriesType = CategoryType[];

export type SortingType = { field: string; order: string };

export type ProductsStateType = {
  products: ProductsType;
  product: NewProductType;
  categories: CategoriesType;
  sorting: SortingType;
  page: number;
  limit: number;
  isProductPageOpen: boolean;
  activeProductUuid: string;
  filter: string;
};

export type GetProductsPayloadType = {
  pagination: { page: number; limit: number };
  sort: { field: string; order: string };
  filter: string;
  fields: string;
};

export type PaginationPageProps = {
  onPageChange: (e: React.MouseEvent<HTMLDivElement>, page: number) => void;
};

export type ActionsTypes =
  | {
      type: typeof SET_PRODUCTS;
      payload: ProductsType;
    }
  | {
      type: typeof SET_FILTER;
      payload: string;
    }
  | {
      type: typeof SET_SORTING;
      payload: SortingType;
    }
  | {
      type: typeof SET_PRODUCT;
      payload: ProductType;
    }
  | {
      type: typeof SET_ACTIVE_PRODUCT_UUID;
      payload: string;
    }
  | {
      type: typeof SET_CATEGORIES;
      payload: CategoriesType;
    }
  | {
      type: typeof SET_ACTIVE_PAGE;
      payload: number;
    }
  | {
      type: typeof SET_IS_PRODUCT_PAGE_OPEN;
      payload: boolean;
    };
