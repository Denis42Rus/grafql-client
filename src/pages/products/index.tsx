import React, { memo, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCategoriesAction, getProductsAction } from "./actions";
import { getIsProductPageOpenSelector } from "./selectors";
import { ProductForm } from "./components/product-form";
import { ProductsList } from "./components/products-list";

const ProductsListPage = memo(() => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProductsAction());
    dispatch(getCategoriesAction());
  }, [dispatch]);

  const IsProductPageOpen: boolean = useSelector(
    getIsProductPageOpenSelector()
  );

  if (IsProductPageOpen) {
    return <ProductForm />;
  }

  return <ProductsList />;
});

export { ProductsListPage };
