/* eslint-disable @typescript-eslint/no-empty-function */
import { runSaga } from "redux-saga";
import {
  getProductSelector,
  getProductsQueryStringSelector,
} from "./selectors";
import {
  addProductSaga,
  deleteProductSaga,
  getCategoriesSaga,
  getProductSaga,
  getProductsSaga,
  updateProductSaga,
} from "./saga";
import {
  addProductRequest,
  deleteProductRequest,
  getCategoriesRequest,
  getProductRequest,
  getProductsRequest,
  updateProductRequest,
} from "../../api/products";
import {
  GET_PRODUCTS,
  SET_ACTIVE_PRODUCT_UUID,
  SET_CATEGORIES,
  SET_IS_PRODUCT_PAGE_OPEN,
  SET_PRODUCT,
  SET_PRODUCTS,
} from "./actions";

jest.mock("../../api/products");

jest.mock("./selectors");

jest.mock("./constants", () => ({
  PRODUCTS_FIELDS: "PRODUCTS_FIELDS",
}));

beforeEach(() => {
  // selectors
  getProductSelector.mockReturnValue(() => "result of getProductSelector");
  getProductsQueryStringSelector.mockReturnValue(() => ({
    pagination: "pagination",
    sort: "sort",
    fields: "fields",
    filter: "filter",
  }));
});

describe("saga", () => {
  describe("getProductsRequest", () => {
    it("should call request with correct props", async () => {
      getProductsRequest.mockResolvedValue({});

      await runSaga(
        { dispatch: () => {}, getState: () => ({}) },
        getProductsSaga,
        {}
      ).toPromise();

      expect(getProductsRequest).toBeCalledWith({
        fields: "fields",
        filter: "filter",
        pagination: "pagination",
        sort: "sort",
      });
    });

    it("should set products list", async () => {
      getProductsRequest.mockResolvedValue({
        data: { products: [] },
      });

      const dispatched = [];

      await runSaga(
        { dispatch: (action) => dispatched.push(action), getState: () => ({}) },
        getProductsSaga,
        {}
      ).toPromise();

      expect(dispatched).toEqual([{ type: SET_PRODUCTS, payload: [] }]);
    });
  });

  describe("getProductSaga", () => {
    it("should call request with correct props", async () => {
      getProductRequest.mockResolvedValue({});

      await runSaga(
        { dispatch: () => {}, getState: () => ({}) },
        getProductSaga,
        { payload: "uuid" }
      ).toPromise();

      expect(getProductRequest).toBeCalledWith({
        uuid: "uuid",
        fields: "PRODUCTS_FIELDS",
      });
    });

    it("should set product", async () => {
      getProductRequest.mockResolvedValue({
        data: {
          product: {
            id: 13,
            category: { title: "category title" },
            title: "Acer SB220Q",
            price: 599,
          },
        },
      });

      const dispatched = [];

      await runSaga(
        { dispatch: (action) => dispatched.push(action), getState: () => ({}) },
        getProductSaga,
        {}
      ).toPromise();

      expect(dispatched).toEqual([
        {
          type: SET_PRODUCT,
          payload: {
            category: "category title",
            id: 13,
            price: 599,
            title: "Acer SB220Q",
          },
        },
      ]);
    });
  });

  describe("getCategoriesSaga", () => {
    it("should call request with correct props", async () => {
      getCategoriesRequest.mockResolvedValue({});

      await runSaga(
        { dispatch: () => {}, getState: () => ({}) },
        getCategoriesSaga,
        {}
      ).toPromise();

      expect(getCategoriesRequest).toBeCalledWith();
    });

    it("should set categories list", async () => {
      getCategoriesRequest.mockResolvedValue({
        data: { categories: [] },
      });

      const dispatched = [];

      await runSaga(
        { dispatch: (action) => dispatched.push(action), getState: () => ({}) },
        getCategoriesSaga,
        {}
      ).toPromise();

      expect(dispatched).toEqual([{ type: SET_CATEGORIES, payload: [] }]);
    });
  });

  describe("addProductSaga", () => {
    it("should call request with correct props", async () => {
      addProductRequest.mockResolvedValue({});

      await runSaga(
        { dispatch: () => {}, getState: () => ({}) },
        addProductSaga,
        {}
      ).toPromise();

      expect(addProductRequest).toBeCalledWith("result of getProductSelector");
    });

    it("should call getProductsAction and close form", async () => {
      addProductRequest.mockResolvedValue({});

      const dispatched = [];

      await runSaga(
        { dispatch: (action) => dispatched.push(action), getState: () => ({}) },
        addProductSaga,
        {}
      ).toPromise();

      expect(dispatched).toEqual([
        { type: GET_PRODUCTS },
        { type: SET_IS_PRODUCT_PAGE_OPEN, payload: false },
        { type: SET_ACTIVE_PRODUCT_UUID, payload: "" },
        { type: SET_PRODUCT, payload: {} },
      ]);
    });
  });

  describe("updateProductSaga", () => {
    it("should call request with correct props", async () => {
      updateProductRequest.mockResolvedValue({});

      await runSaga(
        { dispatch: () => {}, getState: () => ({}) },
        updateProductSaga,
        {}
      ).toPromise();

      expect(updateProductRequest).toBeCalledWith(
        "result of getProductSelector"
      );
    });

    it("should call getProductsAction and close form", async () => {
      updateProductRequest.mockResolvedValue({});

      const dispatched = [];

      await runSaga(
        { dispatch: (action) => dispatched.push(action), getState: () => ({}) },
        updateProductSaga,
        {}
      ).toPromise();

      expect(dispatched).toEqual([
        { type: GET_PRODUCTS },
        { type: SET_IS_PRODUCT_PAGE_OPEN, payload: false },
        { type: SET_ACTIVE_PRODUCT_UUID, payload: "" },
        { type: SET_PRODUCT, payload: {} },
      ]);
    });
  });

  describe("deleteProductSaga", () => {
    it("should call request with correct props", async () => {
      deleteProductRequest.mockResolvedValue({});

      await runSaga(
        { dispatch: () => {}, getState: () => ({}) },
        deleteProductSaga,
        { payload: "uuid" }
      ).toPromise();

      expect(deleteProductRequest).toBeCalledWith("uuid");
    });

    it("should call getProductsAction", async () => {
      deleteProductRequest.mockResolvedValue({
        success: true,
        errors: [],
        headers: {},
        payload: { items: [] },
      });

      const dispatched = [];

      await runSaga(
        { dispatch: (action) => dispatched.push(action), getState: () => ({}) },
        deleteProductSaga,
        {}
      ).toPromise();

      expect(dispatched).toEqual([{ type: GET_PRODUCTS }]);
    });
  });
});
