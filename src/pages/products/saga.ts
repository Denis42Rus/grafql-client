import { call, put, select, takeEvery } from "redux-saga/effects";
import {
  addProductRequest,
  deleteProductRequest,
  getCategoriesRequest,
  getProductRequest,
  getProductsRequest,
  updateProductRequest,
} from "../../api/products";
import {
  ADD_PRODUCT,
  DELETE_PRODUCT,
  GET_CATEGORIES,
  GET_PRODUCT,
  GET_PRODUCTS,
  UPDATE_PRODUCT,
  getProductsAction,
  setActiveProductUuidAction,
  setCategoriesAction,
  setIsProductPageOpenAction,
  setProductAction,
  setProductsAction,
} from "./actions";
import {
  CategoriesType,
  GetProductsPayloadType,
  ProductType,
  ProductFromApiType,
  ProductsType,
} from "./types";
import {
  getProductSelector,
  getProductsQueryStringSelector,
} from "./selectors";
import { PRODUCTS_FIELDS } from "./constants";

export function* getProductsSaga() {
  try {
    const { pagination, sort, fields, filter }: GetProductsPayloadType =
      yield select(getProductsQueryStringSelector());

    const response: { data: { products: ProductsType } } = yield call(
      getProductsRequest,
      { pagination, sort, fields, filter }
    );

    if (response) {
      yield put(setProductsAction(response.data?.products || []));
    } else {
      throw new Error("Error");
    }
  } catch (error) {
    console.log(error);
  }
}

export function* getProductSaga({
  payload: uuid,
}: {
  payload: string;
  type: string;
}) {
  try {
    const response: { data: { product: ProductFromApiType } } = yield call(
      getProductRequest,
      { uuid, fields: PRODUCTS_FIELDS }
    );

    if (response) {
      yield put(
        setProductAction(
          response.data?.product
            ? {
                ...response.data.product,
                category: response.data.product.category.title,
              }
            : {}
        )
      );
    } else {
      throw new Error("Error");
    }
  } catch (error) {
    console.log(error);
  }
}

export function* getCategoriesSaga() {
  try {
    const response: { data: { categories: CategoriesType } } = yield call(
      getCategoriesRequest
    );

    if (response) {
      yield put(setCategoriesAction(response.data?.categories || []));
    } else {
      throw new Error("Error");
    }
  } catch (error) {
    console.log(error);
  }
}

export function* addProductSaga() {
  try {
    const payload: ProductType = yield select(getProductSelector());

    const response: { data: { product: ProductType } } = yield call(
      addProductRequest,
      payload
    );

    if (response) {
      yield put(getProductsAction());
      yield put(setIsProductPageOpenAction(false));
      yield put(setActiveProductUuidAction(""));
      yield put(setProductAction({}));
    } else {
      throw new Error("Error");
    }
  } catch (error) {
    console.log(error);
  }
}

export function* updateProductSaga() {
  try {
    const payload: ProductType = yield select(getProductSelector());

    const response: { data: { product: ProductType } } = yield call(
      updateProductRequest,
      payload
    );

    if (response) {
      yield put(getProductsAction());
      yield put(setIsProductPageOpenAction(false));
      yield put(setActiveProductUuidAction(""));
      yield put(setProductAction({}));
    } else {
      throw new Error("Error");
    }
  } catch (error) {
    console.log(error);
  }
}

export function* deleteProductSaga({
  payload: uuid,
}: {
  payload: string;
  type: string;
}) {
  try {
    const response: { data: { product: ProductFromApiType } } = yield call(
      deleteProductRequest,
      uuid
    );

    if (response) {
      yield put(getProductsAction());
    } else {
      throw new Error("Error");
    }
  } catch (error) {
    console.log(error);
  }
}

export default function* productsSaga() {
  yield takeEvery(GET_PRODUCTS, getProductsSaga);
  yield takeEvery(GET_CATEGORIES, getCategoriesSaga);
  yield takeEvery(ADD_PRODUCT, addProductSaga);
  yield takeEvery(GET_PRODUCT, getProductSaga);
  yield takeEvery(UPDATE_PRODUCT, updateProductSaga);
  yield takeEvery(DELETE_PRODUCT, deleteProductSaga);
}
