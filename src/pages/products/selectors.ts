import { createSelector } from "reselect";
import { ProductsStateType } from "./types";
import { PRODUCTS_FIELDS } from "./constants";

export const getRoutesScreenStateSelector = (state: ProductsStateType) => state;

export const getProductsSelector = () =>
  createSelector(
    getRoutesScreenStateSelector,
    ({ products }: ProductsStateType) => products
  );

export const getProductSelector = () =>
  createSelector(
    getRoutesScreenStateSelector,
    ({ product }: ProductsStateType) => product
  );

export const getProductsQueryStringSelector = () =>
  createSelector(
    getRoutesScreenStateSelector,
    ({ page, limit, sorting, filter }: ProductsStateType) => ({
      pagination: { page, limit },
      sort: sorting,
      filter,
      fields: PRODUCTS_FIELDS,
    })
  );

export const getIsProductPageOpenSelector = () =>
  createSelector(
    getRoutesScreenStateSelector,
    ({ isProductPageOpen }: ProductsStateType) => isProductPageOpen
  );

export const getCategoriesSelector = () =>
  createSelector(
    getRoutesScreenStateSelector,
    ({ categories }: ProductsStateType) => categories
  );

export const getActiveProductUuidSelector = () =>
  createSelector(
    getRoutesScreenStateSelector,
    ({ activeProductUuid }: ProductsStateType) => activeProductUuid
  );

export const getSortingSelector = () =>
  createSelector(
    getRoutesScreenStateSelector,
    ({ sorting }: ProductsStateType) => sorting
  );
