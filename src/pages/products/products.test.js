/* eslint-disable react/react-in-jsx-scope */
import { render, screen } from "@testing-library/react";
import { getIsProductPageOpenSelector } from "./selectors";
import { GET_CATEGORIES, GET_PRODUCTS } from "./actions";
import { ProductsListPage } from ".";

jest.mock("./components/product-form", () => ({
  __esModule: true,
  ProductForm: () => {
    return <div data-testid="ProductForm" />;
  },
}));

jest.mock("./components/products-list", () => ({
  __esModule: true,
  ProductsList: () => {
    return <div data-testid="ProductsList" />;
  },
}));

const mockDispatch = jest.fn();

jest.mock("react-redux", () => ({
  __esModule: true,
  ...jest.requireActual("react-redux"),
  useDispatch: () => mockDispatch,
  useSelector: (cb) => cb?.(),
}));

jest.mock("./selectors");

beforeEach(() => {
  getIsProductPageOpenSelector.mockReturnValue(() => false);
});

describe("Products", () => {
  it("should call actions on mount", () => {
    render(<ProductsListPage />);

    expect(mockDispatch).nthCalledWith(1, { type: GET_PRODUCTS });
    expect(mockDispatch).nthCalledWith(2, { type: GET_CATEGORIES });
  });

  it("should render product list", () => {
    render(<ProductsListPage />);

    expect(screen.getByTestId("ProductsList")).toBeInTheDocument();
  });

  it("should render product form", () => {
    getIsProductPageOpenSelector.mockReturnValue(() => true);
    render(<ProductsListPage />);

    expect(screen.getByTestId("ProductForm")).toBeInTheDocument();
  });
});
