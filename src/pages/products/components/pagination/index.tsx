import React from "react";
import { PaginationView } from "./pagination-view";
import { useDispatch } from "react-redux";
import { getProductsAction, setActivePageAction } from "../../actions";

const Pagination = () => {
  const dispatch = useDispatch();

  const handlePageChange = (
    _: React.MouseEvent<HTMLDivElement>,
    page: number
  ) => {
    dispatch(setActivePageAction(page));
    dispatch(getProductsAction());
  };

  return <PaginationView onPageChange={handlePageChange} />;
};

export { Pagination };
