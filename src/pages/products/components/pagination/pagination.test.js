import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import renderer from "react-test-renderer";
import { Pagination } from ".";
import { GET_PRODUCTS, SET_ACTIVE_PAGE } from "../../actions";

const mockDispatch = jest.fn();

jest.mock("react-redux", () => ({
  __esModule: true,
  ...jest.requireActual("react-redux"),
  useDispatch: () => mockDispatch,
}));

describe("Pagination", () => {
  it("should call handlePageChange", () => {
    render(<Pagination />);

    userEvent.click(document.querySelectorAll(".pagination__item")[1]);

    expect(mockDispatch).nthCalledWith(1, {
      payload: 2,
      type: SET_ACTIVE_PAGE,
    });
    expect(mockDispatch).nthCalledWith(2, { type: GET_PRODUCTS });
  });

  it("snapshot test ", () => {
    const component = renderer.create(<Pagination />);

    expect(component.toJSON()).toMatchSnapshot();
  });
});
