import React from "react";
import "./styles.scss";
import { PaginationPageProps } from "../../../types";

const PaginationView = ({ onPageChange }: PaginationPageProps) => {
  return (
    <div className="pagination">
      <div onClick={(e) => onPageChange(e, 1)} className="pagination__item">
        1
      </div>
      <div onClick={(e) => onPageChange(e, 2)} className="pagination__item">
        2
      </div>
      <div onClick={(e) => onPageChange(e, 3)} className="pagination__item">
        3
      </div>
    </div>
  );
};

export { PaginationView };
