import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import renderer from "react-test-renderer";
import { Filters } from ".";
import { SET_FILTER, SET_SORTING } from "../../actions";
import { getCategoriesSelector, getSortingSelector } from "../../selectors";

const mockDispatch = jest.fn();

jest.mock("react-redux", () => ({
  __esModule: true,
  ...jest.requireActual("react-redux"),
  useDispatch: () => mockDispatch,
  useSelector: (cb) => cb?.(),
}));

jest.mock("../../selectors");

beforeEach(() => {
  getSortingSelector.mockReturnValue(() => ({ field: "title", order: "asc" }));
  getCategoriesSelector.mockReturnValue(() => [
    { id: 1, title: "men's clothing" },
    { id: 2, title: "women's clothing" },
    { id: 3, title: "jewelery" },
    { id: 4, title: "electronics" },
  ]);
});

describe("filters", () => {
  it("should call handleSortingChange with 'price'", () => {
    render(<Filters />);

    userEvent.selectOptions(
      screen.getByTestId("filters-field-select"),
      "price"
    );

    expect(mockDispatch).nthCalledWith(1, {
      payload: { field: "price", order: "asc" },
      type: SET_SORTING,
    });
    expect(mockDispatch).nthCalledWith(2, { type: "GET_PRODUCTS" });
  });

  it("should call handleSortingChange with 'desc'", () => {
    render(<Filters />);

    userEvent.selectOptions(screen.getByTestId("filters-order-select"), "desc");

    expect(mockDispatch).nthCalledWith(1, {
      payload: { order: "desc", field: "title" },
      type: SET_SORTING,
    });
    expect(mockDispatch).nthCalledWith(2, { type: "GET_PRODUCTS" });
  });

  it("should call handleFilterChange", () => {
    render(<Filters />);

    userEvent.selectOptions(screen.getByTestId("filters-filter-select"), "2");

    expect(mockDispatch).nthCalledWith(1, { payload: "2", type: SET_FILTER });
    expect(mockDispatch).nthCalledWith(2, { type: "GET_PRODUCTS" });
  });

  it("snapshot test ", () => {
    const component = renderer.create(<Filters />);

    expect(component.toJSON()).toMatchSnapshot();
  });
});
