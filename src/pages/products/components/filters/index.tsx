import React from "react";
import { CategoriesType, SortingType } from "../../types";
import { FiltersView } from "./filters-view";
import { useDispatch, useSelector } from "react-redux";
import {
  getProductsAction,
  setFilterAction,
  setSortingAction,
} from "../../actions";
import { getCategoriesSelector, getSortingSelector } from "../../selectors";
const Filters = () => {
  const dispatch = useDispatch();

  const sorting: SortingType = useSelector(getSortingSelector());

  const categories: CategoriesType = useSelector(getCategoriesSelector());

  const handleSortingChange = (
    e: React.ChangeEvent<HTMLSelectElement>,
    field: string
  ) => {
    dispatch(setSortingAction({ ...sorting, [field]: e.target.value }));
    dispatch(getProductsAction());
  };

  const handleFilterChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    dispatch(setFilterAction(e.target.value));
    dispatch(getProductsAction());
  };

  return (
    <FiltersView
      categories={categories}
      onSortingChange={handleSortingChange}
      onFilterChange={handleFilterChange}
    />
  );
};

export { Filters };
