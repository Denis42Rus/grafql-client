import React from "react";
import "./styles.scss";
import { CategoriesType } from "../../../types";
const FiltersView = ({
  onSortingChange,
  onFilterChange,
  categories,
}: {
  onSortingChange: (
    e: React.ChangeEvent<HTMLSelectElement>,
    field: string
  ) => void;
  onFilterChange: (e: React.ChangeEvent<HTMLSelectElement>) => void;
  categories: CategoriesType;
}) => {
  return (
    <div className="filters">
      <div className="filters__item">
        <span className="filters__item-title">Поле сортировки</span>
        <select
          data-testid="filters-field-select"
          className="filters__select"
          onChange={(e) => onSortingChange(e, "field")}
        >
          <option value="title">Title</option>
          <option value="price">Price</option>
        </select>
      </div>
      <div className="filters__item">
        <span className="filters__item-title">Направление сортировки</span>
        <select
          data-testid="filters-order-select"
          className="filters__select"
          onChange={(e) => onSortingChange(e, "order")}
        >
          <option value="asc">asc</option>
          <option value="desc">desc</option>
        </select>
      </div>
      <div className="filters__item">
        <span className="filters__item-title">Категория</span>
        <select
          data-testid="filters-filter-select"
          className="filters__select"
          onChange={onFilterChange}
        >
          <option value=""></option>
          {categories?.map((category) => (
            <option value={category.id} key={category.id}>
              {category.title}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};

export { FiltersView };
