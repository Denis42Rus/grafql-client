import React from "react";
import "./styles.scss";
import { ProductType, ProductsType } from "../../../types";
import { Pagination } from "../../pagination";
import { Header } from "../../header";
import { Filters } from "../../filters";

const ProductsListView = ({
  products,
  onProductPageOpen,
  onProductDblClick,
  onProductDelete,
}: {
  products: ProductsType;
  onProductPageOpen: () => void;
  onProductDblClick: (uuid: string) => void;
  onProductDelete: (uuid: string) => void;
}) => {
  return (
    <div className="products">
      <Header
        onProductPageOpen={onProductPageOpen}
        title="Products"
        buttonText="add new"
      />
      <div className="products__main">
        <Filters />
        <div className="products__content">
          <ul className="products__list">
            {products.map((product: ProductType) => (
              <div
                className="products__item"
                key={product.id}
                onDoubleClick={() => onProductDblClick(product.id)}
              >
                <div>{product.title}</div>
                <div className="products__button-wrapper">
                  <div>{product.price}</div>
                  <input
                    data-testid="product-delete-button"
                    type="button"
                    value="Delete"
                    onClick={() => onProductDelete(product.id)}
                  />
                </div>
              </div>
            ))}
          </ul>
          <Pagination />
        </div>
      </div>
    </div>
  );
};

export { ProductsListView };
