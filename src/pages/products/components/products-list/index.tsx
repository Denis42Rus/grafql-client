import React from "react";
import { ProductsListView } from "./products-list-view";
import { useDispatch, useSelector } from "react-redux";
import { getProductsSelector } from "../../selectors";
import {
  deleteProductAction,
  getProductAction,
  setActiveProductUuidAction,
  setIsProductPageOpenAction,
} from "../../actions";
import { ProductsType } from "../../types";

const ProductsList = () => {
  const dispatch = useDispatch();

  const products: ProductsType = useSelector(getProductsSelector());

  const handleProductPageOpen = () => {
    dispatch(setIsProductPageOpenAction(true));
  };

  const handleProductDblClick = (uuid: string) => {
    dispatch(setActiveProductUuidAction(uuid));
    dispatch(getProductAction(uuid));
    dispatch(setIsProductPageOpenAction(true));
  };

  const handleProductDelete = (uuid: string) => {
    dispatch(deleteProductAction(uuid));
  };

  return (
    <ProductsListView
      products={products}
      onProductPageOpen={handleProductPageOpen}
      onProductDblClick={handleProductDblClick}
      onProductDelete={handleProductDelete}
    />
  );
};

export { ProductsList };
