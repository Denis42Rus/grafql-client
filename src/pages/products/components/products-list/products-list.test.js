/* eslint-disable react/react-in-jsx-scope */
import { render, screen } from "@testing-library/react";
import renderer from "react-test-renderer";
import userEvent from "@testing-library/user-event";
import {
  getCategoriesSelector,
  getIsProductPageOpenSelector,
  getProductsSelector,
} from "../../selectors";
import { ProductsList } from ".";
import {
  DELETE_PRODUCT,
  GET_PRODUCT,
  SET_ACTIVE_PRODUCT_UUID,
  SET_IS_PRODUCT_PAGE_OPEN,
} from "../../actions";

jest.mock("../filters", () => ({
  __esModule: true,
  Filters: () => {
    return <div data-testid="Filters" />;
  },
}));

jest.mock("../pagination", () => ({
  __esModule: true,
  Pagination: () => {
    return <div data-testid="Pagination" />;
  },
}));

const mockDispatch = jest.fn();

jest.mock("react-redux", () => ({
  __esModule: true,
  ...jest.requireActual("react-redux"),
  useDispatch: () => mockDispatch,
  useSelector: (cb) => cb?.(),
}));

jest.mock("../../selectors");

beforeEach(() => {
  getIsProductPageOpenSelector.mockReturnValue(() => false);
  getProductsSelector.mockReturnValue(() => [
    { title: "product-1", price: 1, id: 1 },
    { title: "product-2", price: 2, id: 2 },
    { title: "product-3", price: 3, id: 3 },
  ]);
  getCategoriesSelector.mockReturnValue(() => [
    { id: 1, title: "men's clothing" },
    { id: 2, title: "women's clothing" },
    { id: 3, title: "jewelery" },
    { id: 4, title: "electronics" },
  ]);
});

describe("products-view", () => {
  it("should render filters", () => {
    render(<ProductsList />);

    expect(screen.getByTestId("Filters")).toBeInTheDocument();
  });

  it("should render pagination", () => {
    render(<ProductsList />);

    expect(screen.getByTestId("Pagination")).toBeInTheDocument();
  });

  it("should call handleProductPageOpen", () => {
    render(<ProductsList />);

    userEvent.click(screen.getByTestId("header-button"));

    expect(mockDispatch).nthCalledWith(1, {
      payload: true,
      type: SET_IS_PRODUCT_PAGE_OPEN,
    });
  });

  it("should call handleProductDblClick", () => {
    render(<ProductsList />);

    userEvent.dblClick(screen.getByText("product-1"));

    expect(mockDispatch).nthCalledWith(1, {
      payload: 1,
      type: SET_ACTIVE_PRODUCT_UUID,
    });
    expect(mockDispatch).nthCalledWith(2, {
      payload: 1,
      type: GET_PRODUCT,
    });
    expect(mockDispatch).nthCalledWith(3, {
      payload: true,
      type: SET_IS_PRODUCT_PAGE_OPEN,
    });
  });

  it("should call handleProductDelete", () => {
    render(<ProductsList />);

    userEvent.click(screen.getAllByTestId("product-delete-button")[0]);

    expect(mockDispatch).nthCalledWith(1, {
      payload: 1,
      type: DELETE_PRODUCT,
    });
  });

  it("snapshot test", () => {
    const component = renderer.create(<ProductsList />);

    expect(component.toJSON()).toMatchSnapshot();
  });
});
