import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import renderer from "react-test-renderer";
import { ProductForm } from ".";
import {
  ADD_PRODUCT,
  SET_ACTIVE_PRODUCT_UUID,
  SET_IS_PRODUCT_PAGE_OPEN,
  SET_PRODUCT,
  UPDATE_PRODUCT,
} from "../../actions";
import {
  getActiveProductUuidSelector,
  getCategoriesSelector,
  getProductSelector,
} from "../../selectors";

const mockDispatch = jest.fn();

jest.mock("react-redux", () => ({
  __esModule: true,
  ...jest.requireActual("react-redux"),
  useDispatch: () => mockDispatch,
  useSelector: (cb) => cb?.(),
}));

jest.mock("../../selectors");

beforeEach(() => {
  getActiveProductUuidSelector.mockReturnValue(() => "product uuid");
  getProductSelector.mockReturnValue(() => ({
    title: "product-1",
    price: 1,
    id: 1,
  }));
  getCategoriesSelector.mockReturnValue(() => [
    { id: 1, title: "men's clothing" },
    { id: 2, title: "women's clothing" },
    { id: 3, title: "jewelery" },
    { id: 4, title: "electronics" },
  ]);
});

describe("product-form", () => {
  it("should call handleProductPageOpen", () => {
    render(<ProductForm />);

    userEvent.click(screen.getByTestId("header-button"));

    expect(mockDispatch).nthCalledWith(1, {
      payload: false,
      type: SET_IS_PRODUCT_PAGE_OPEN,
    });
    expect(mockDispatch).nthCalledWith(2, {
      payload: "",
      type: SET_ACTIVE_PRODUCT_UUID,
    });
    expect(mockDispatch).nthCalledWith(3, { payload: {}, type: SET_PRODUCT });
  });

  it("should not call setActiveProductUuidAction if activeProductUuid is empty", () => {
    getActiveProductUuidSelector.mockReturnValue(() => "");
    render(<ProductForm />);

    userEvent.click(screen.getByTestId("header-button"));

    expect(mockDispatch).nthCalledWith(1, {
      payload: false,
      type: SET_IS_PRODUCT_PAGE_OPEN,
    });
    expect(mockDispatch).nthCalledWith(2, { payload: {}, type: SET_PRODUCT });
  });

  it("should call updateProductAction", () => {
    render(<ProductForm />);

    userEvent.click(document.querySelector(".product-form__button"));

    expect(mockDispatch).nthCalledWith(1, { type: UPDATE_PRODUCT });
  });

  it("should call addProductAction", () => {
    getActiveProductUuidSelector.mockReturnValue(() => "");

    render(<ProductForm />);

    userEvent.click(document.querySelector(".product-form__button"));

    expect(mockDispatch).nthCalledWith(1, { type: ADD_PRODUCT });
  });

  it("should call handleProductChange with title field", () => {
    getProductSelector.mockReturnValue(() => ({}));

    render(<ProductForm />);

    userEvent.type(screen.getByTestId("product-title-field"), "1");

    expect(mockDispatch).nthCalledWith(1, {
      type: SET_PRODUCT,
      payload: { title: "1" },
    });
  });

  it("should call handleProductChange with price field", () => {
    getProductSelector.mockReturnValue(() => ({}));

    render(<ProductForm />);

    userEvent.type(screen.getByTestId("product-price-field"), "1");

    expect(mockDispatch).nthCalledWith(1, {
      type: SET_PRODUCT,
      payload: { price: "1" },
    });
  });

  it("should call handleProductChange with category field", () => {
    getProductSelector.mockReturnValue(() => ({}));

    render(<ProductForm />);

    userEvent.selectOptions(
      screen.getByTestId("product-category-field"),
      "jewelery"
    );

    expect(mockDispatch).nthCalledWith(1, {
      type: SET_PRODUCT,
      payload: { category: "3" },
    });
  });

  it("should render header", () => {
    render(<ProductForm />);

    expect(screen.getByTestId("header-button")).toHaveAttribute(
      "value",
      "back"
    );
    expect(screen.getByText("Product")).toBeInTheDocument();
  });

  it("snapshot test ", () => {
    const component = renderer.create(<ProductForm />);

    expect(component.toJSON()).toMatchSnapshot();
  });
});
