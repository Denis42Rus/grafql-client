import React from "react";
import "./styles.scss";
import { CategoriesType, NewProductType } from "../../../types";
import { Header } from "../../header";
const ProductFormView = ({
  onProductPageOpen,
  categories,
  onProductAdd,
  onProductChange,
  product,
}: {
  onProductPageOpen: () => void;
  categories: CategoriesType;
  onProductAdd: () => void;
  onProductChange: (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>,
    field: string
  ) => void;
  product: NewProductType;
}) => {
  return (
    <div className="product-form">
      <Header
        onProductPageOpen={onProductPageOpen}
        title="Product"
        buttonText="back"
      />
      <div className="product-form__content">
        <div className="product-form__row">
          <input
            data-testid="product-title-field"
            className="product-form__field"
            type="text"
            placeholder="title"
            value={product.title}
            onChange={(e) => onProductChange(e, "title")}
          />
          <input
            data-testid="product-price-field"
            className="product-form__field"
            type="number"
            placeholder="price"
            value={product.price}
            onChange={(e) => onProductChange(e, "price")}
          />
          <select
            data-testid="product-category-field"
            onChange={(e) => onProductChange(e, "category")}
            className="product-form__field"
          >
            {categories.map((category) => (
              <option
                selected={category.title === product.category}
                value={category.id}
                key={category.id}
              >
                {category.title}
              </option>
            ))}
          </select>
        </div>
        <div className="product-form__button-wrapper">
          <input
            className="product-form__button"
            type="button"
            value="Submit"
            onClick={onProductAdd}
          />
        </div>
      </div>
    </div>
  );
};

export { ProductFormView };
