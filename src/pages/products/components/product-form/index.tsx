import React from "react";
import { CategoriesType, NewProductType } from "../../types";
import { ProductFormView } from "./product-form-view";
import { useDispatch, useSelector } from "react-redux";
import {
  getActiveProductUuidSelector,
  getCategoriesSelector,
  getProductSelector,
} from "../../selectors";
import {
  addProductAction,
  setActiveProductUuidAction,
  setIsProductPageOpenAction,
  setProductAction,
  updateProductAction,
} from "../../actions";
const ProductForm = () => {
  const dispatch = useDispatch();

  const activeProductUuid: string = useSelector(getActiveProductUuidSelector());

  const product: NewProductType = useSelector(getProductSelector());

  const categories: CategoriesType = useSelector(getCategoriesSelector());

  const handleProductPageOpen = () => {
    dispatch(setIsProductPageOpenAction(false));
    if (activeProductUuid) {
      dispatch(setActiveProductUuidAction(""));
    }
    dispatch(setProductAction({}));
  };

  const handleProductAdd = () => {
    if (activeProductUuid) {
      dispatch(updateProductAction());
    } else {
      dispatch(addProductAction());
    }
  };

  const handleProductChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>,
    field: string
  ) => {
    dispatch(setProductAction({ ...product, [field]: e.target.value }));
  };

  return (
    <ProductFormView
      onProductPageOpen={handleProductPageOpen}
      categories={categories}
      onProductAdd={handleProductAdd}
      onProductChange={handleProductChange}
      product={product}
    />
  );
};

export { ProductForm };
