import { render, screen } from "@testing-library/react";
import renderer from "react-test-renderer";
import { Header } from ".";

describe("Header", () => {
  it("snapshot test ", () => {
    const component = renderer.create(
      <Header
        onProductPageOpen={() => {}}
        title="title"
        buttonText="buttonText"
      />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });
});
