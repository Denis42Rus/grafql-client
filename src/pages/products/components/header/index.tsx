import React from "react";
import "./styles.scss";
const Header = ({
  onProductPageOpen,
  title,
  buttonText,
}: {
  onProductPageOpen: () => void;
  title: string;
  buttonText: string;
}) => {
  return (
    <div className="header">
      <div>{title}</div>
      <input
        data-testid="header-button"
        onClick={onProductPageOpen}
        type="button"
        value={buttonText}
      />
    </div>
  );
};

export { Header };
