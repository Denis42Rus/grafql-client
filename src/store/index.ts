import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import productReducer from "../pages/products/reducer";
import rootSaga from "./saga";

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(
  productReducer,
  applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);
