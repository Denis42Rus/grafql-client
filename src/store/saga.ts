import { all } from "redux-saga/effects";
import productsSaga from "../pages/products/saga";

export default function* rootSaga() {
  yield all([productsSaga()]);
}
