import { url } from "./constants";

export const makeRequest = async ({
  query,
}: {
  query: string;
}): Promise<unknown> => {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers: { "content-type": "application/json" },
      body: JSON.stringify({ query, variables: null }),
    });

    const json = await response.json();

    return json;
  } catch (error) {
    return error;
  }
};
