export type GetProductsPayloadType = {
  pagination: { page: number; limit: number };
  sort: { field: string; order: string };
  filter: string;
  fields: string;
};

export type Product = {
  category?: string;
  price?: number;
  title?: string;
  id?: string;
};
