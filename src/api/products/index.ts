import { handleError } from "../../modules/error-handler";
import { makeRequest } from "../makeRequests";
import { GetProductsPayloadType, Product } from "./types";

export const getProductsRequest = async ({
  sort,
  pagination,
  fields,
  filter,
}: GetProductsPayloadType) => {
  try {
    const response = await makeRequest({
      query: `query {
        products( sort: {field: "${sort.field}", order: "${sort.order}"}, pagination: { page: ${pagination.page}, limit: ${pagination.limit} }, filters: { field: "${filter}"} ) { ${fields} }
      }
    `,
    });

    return response;
  } catch (error) {
    handleError(error);
  }
};

export const getProductRequest = async ({
  uuid,
  fields,
}: {
  uuid: string;
  fields: string;
}) => {
  try {
    const response = await makeRequest({
      query: `query { product( id: "${uuid}" ) { ${fields} } } `,
    });

    return response;
  } catch (error) {
    handleError(error);
  }
};

export const getCategoriesRequest = async () => {
  try {
    const response = await makeRequest({
      query: `query { categories { id title } } `,
    });

    return response;
  } catch (error) {
    handleError(error);
  }
};

export const addProductRequest = async (product: Product) => {
  try {
    const response = await makeRequest({
      query: `mutation { addProduct (title: "${product.title}", category: "${product.category}", price: ${product.price}) { id title } } `,
    });

    return response;
  } catch (error) {
    handleError(error);
  }
};

export const updateProductRequest = async (product: Product) => {
  try {
    const response = await makeRequest({
      query: `mutation { updateProduct (title: "${product.title}", category: "${product.category}", price: ${product.price}, id: "${product.id}") { id title } } `,
    });

    return response;
  } catch (error) {
    handleError(error);
  }
};

export const deleteProductRequest = async (uuid: string) => {
  try {
    const response = await makeRequest({
      query: `mutation { deleteProduct( id: "${uuid}" ) { id title } } `,
    });

    return response;
  } catch (error) {
    handleError(error);
  }
};
